export interface Amiibo {
    id: number;
    amiiboSeries: string;
    character: string;
    gameSeries: string;
    image: string;
    name: string;
}