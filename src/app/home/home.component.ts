import { Component, OnInit } from "@angular/core";
import { Amiibo } from './amiibo.model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    amiibo: Amiibo[] = [];
    isLoading = false;

    constructor(){}

    getAmiibo(){
        this.isLoading = true;
        this.amiibo = [];
        let random = () => Math.floor(Math.random() * 101);
        let getData = async () => {
            let url = "https://amiiboapi.com/api/amiibo/";
            let res = await fetch(url);
            let data = await res.json();
            let arr = data.amiibo;

            for (let i = 0; i < 3; i++) {
                let id = random();
                let {amiiboSeries, character, gameSeries, image, name} = arr[id];
                this.amiibo[i] = { id, amiiboSeries, character, gameSeries, image, name};
            }
            this.isLoading = false;
        }
        getData();
        
    }

    ngOnInit(){
        this.isLoading = true;
    }
}